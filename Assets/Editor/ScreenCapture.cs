﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class Extras {

    [MenuItem("Extras/Clean Player Prefs")]
    public static void CleanPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }

    [MenuItem("Extras/Screen Shot")]
    public static void ScreenShot()
    {
        string path = "C:/Users/LUCAS/Desktop/";
        string filename = "SS";
        string format = ".png";

        if ( ! File.Exists(path + filename + format))
        {
            Debug.Log(path);
            ScreenCapture.CaptureScreenshot(path + filename + format);
            Debug.Log("done =)");
            return;
        }

        filename = "SS_";

        for (int i = 1; i < 100; i++)
        {
            if( ! File.Exists(path + filename + i.ToString() + format))
            {
                Debug.Log(path);
                ScreenCapture.CaptureScreenshot(path + filename + i.ToString() + format);
                Debug.Log("done =)");
                break;
            }
        }
    }
}